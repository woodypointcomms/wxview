import {Component, OnInit} from '@angular/core';

  import {Paho} from 'ng2-mqtt/mqttws31';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {

  windSpeed: number;
  windDirection: number;
  rain: number;
  tempC: number;
  dewpoint: number;
  humidity: number;
  airpressure: number;
  private client;

  mqttbroker = '10.2.69.8';

    ngOnInit() {
      this.client = new Paho.MQTT.Client(this.mqttbroker, Number(9001), 'wxview');
      this.client.onMessageArrived = this.onMessageArrived.bind(this);
      this.client.onConnectionLost = this.onConnectionLost.bind(this);
      this.client.connect({onSuccess: this.onConnect.bind(this)});
    }

  onConnect() {
    console.log('onConnect');
    this.client.subscribe('wxstation/wind_speed');
    this.client.subscribe('wxstation/wind_direction');
    this.client.subscribe('wxstation/rain');
    this.client.subscribe('wxstation/tempC');
    this.client.subscribe('wxstation/humidityPc');
    this.client.subscribe('wxstation/pressurePa');
  }

  onConnectionLost(responseObject) {
    if (responseObject.errorCode !== 0) {
      console.log('onConnectionLost:' + responseObject.errorMessage);
    }
  }

  onMessageArrived(message) {
    console.log('onMessageArrived: ' + message.destinationName + ': ' + message.payloadString);

    if (message.destinationName.indexOf('wind_speed') !== -1) {
      this.windSpeed = parseFloat(message.payloadString);
    }

    if (message.destinationName.indexOf('wind_direction') !== -1) {
      this.windDirection = parseInt(message.payloadString);
    }

    if (message.destinationName.indexOf('rain') !== -1) {
      const tempRain = parseFloat(message.payloadString);
      this.rain = Math.round( tempRain * 10) / 10;
    }

    if (message.destinationName.indexOf('tempC') !== -1) {
      this.tempC = parseFloat(message.payloadString);
    }

    if (message.destinationName.indexOf('humidityPc') !== -1) {
      this.humidity = parseFloat(message.payloadString);

      const tempDp = 243.04 * (Math.log(this.humidity / 100) +
        (17.625 * this.tempC) / (243.04 + this.tempC)) /
        (17.625 - Math.log(this.humidity / 100) - ((17.625 * this.tempC) /
          (243.04 + this.tempC)));
      this.dewpoint = Math.round( tempDp * 10) / 10;

    }

    if (message.destinationName.indexOf('pressurePa') !== -1) {
      const tempPressure = message.payloadString / 100;
      this.airpressure =  Math.round( tempPressure * 10) / 10;
    }

  }

}
